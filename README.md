# Docker Postgres XL

This is an unofficial Debian build of http://www.postgres-xl.org/.


## Images

* Dockerfile.base -> https://hub.docker.com/r/mziescha/postgres-xl-base/
* Dockerfile.coordinator -> https://hub.docker.com/r/mziescha/postgres-xl-coordinator/
* Dockerfile.datanode -> https://hub.docker.com/r/mziescha/postgres-xl-datanode/
* Dockerfile.gtm (global transaction manager) -> https://hub.docker.com/r/mziescha/postgres-xl-gtm/

## Usage

    sudo rm -rf /db/*
    cd ~/data/workspace/Docker/docker-postgres-xl-ansible-kubernetes/
    ansible-playbook ansible/playbooks/build_images_local.yml
    ansible-playbook ansible/playbooks/pgxl_startup_local_docker.yml
    docker exec -it -w /App datanode_1_local perl -Ilib ./bin/ppostgresxl node
