FROM mziescha/postgres-xl-base

ENV PG_GTM_HOST=0.0.0.0
ENV PG_GTM_PORT=6666
ENV export dataDirRoot=$PGDATA/nodes

COPY init/gtm.sh /usr/local/bin/gtm.sh

ENTRYPOINT ["gtm.sh"]

EXPOSE ${PG_GTM_PORT}
