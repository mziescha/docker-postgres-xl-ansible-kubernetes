FROM mziescha/postgres-xl-base

ENV PG_DATA_HOST=0.0.0.0
ENV PG_DATA_PORT=5432
ENV PG_GTM_PORT=6666
ENV PG_USER_HEALTHCHECK=_healthcheck
ENV GTM_HOST=127.0.0.1

RUN yes | cpan -i App::cpanminus DDP

COPY init/data.sh /usr/local/bin/data.sh
COPY init/App-PostgresXL /root/App-PostgresXL

RUN  cd /root/App-PostgresXL && \
     cpanm -q --installdeps . && \
     perl Makefile.PL && \
     make && \
     make test && \
     make install

ENTRYPOINT ["data.sh"]

EXPOSE ${PG_DATA_PORT}
