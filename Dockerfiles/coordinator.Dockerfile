FROM mziescha/postgres-xl-base

ENV PG_COORD_HOST=0.0.0.0
ENV PG_COORD_PORT=5432
ENV PG_GTM_PORT=6666
ENV PG_USER_HEALTHCHECK=_healthcheck
ENV GTM_HOST=127.0.0.1

RUN yes | cpan -i App::cpanminus DDP

COPY init/coordinator.sh /usr/local/bin/coordinator.sh
COPY init/App-PostgresXL /root/App-PostgresXL

RUN  cd /root/App-PostgresXL && \
     cpanm -q --installdeps . && \
     perl Makefile.PL && \
     make && \
     make test && \
     make install

EXPOSE ${PG_COORD_PORT}

ENTRYPOINT ["coordinator.sh"]
