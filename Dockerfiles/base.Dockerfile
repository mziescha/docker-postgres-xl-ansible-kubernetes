FROM ubuntu:18.04

ARG DEBIAN_FRONTEND=noninteractive

ARG POSTGRES_XL_VERSION=postgres-xl-10r1.1

ENV PG_HOME /var/lib/postgresql
ENV PG_LIB /usr/local/lib/postgresql
ENV PG_USER postgres
ENV PG_GROUP postgres
ENV PATH ${PG_LIB}/bin:${PATH}
ENV PGDATA ${PG_HOME}/data
ENV GOSU_VERSION 1.10
ENV PG_GID 125
ENV PG_UID 125

RUN       apt-get -q update \
      &&  apt-get install -y \
          wget apt-utils build-essential libreadline-dev zlib1g-dev perl libperl-dev \
          libipc-run-perl flex bison libxml2-dev libxslt-dev vim libssl-dev libc++-dev \
          locales ca-certificates net-tools iproute2 \
      &&  localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8 \
      &&  groupadd -r ${PG_GROUP} --gid=${PG_GID} \
      &&  useradd -r -g ${PG_GROUP} --uid=${PG_UID} ${PG_USER} -d ${PG_HOME} \
      &&  mkdir -p $HOME/.ssh \
      &&  touch $HOME/.ssh/authorized_keys \
      &&  chmod 600 $HOME/.ssh/authorized_keys \
      &&  dpkgArch="$(dpkg --print-architecture | awk -F- '{ print $NF }')" \
      &&  wget -O /usr/local/bin/gosu "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-$dpkgArch" \
      &&  chmod +x /usr/local/bin/gosu \
      && gosu nobody true

ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

RUN       export TMP_PG=$(mktemp -d) \
      &&  cd $TMP_PG \
      &&  wget https://www.postgres-xl.org/downloads/postgres-xl-10r1.1.tar.gz \
      &&  tar -xf ${POSTGRES_XL_VERSION}.tar.gz \
      &&  cd ${POSTGRES_XL_VERSION} \
      &&  ./configure --prefix ${PG_LIB} --with-openssl --with-perl --enable-tap-tests \
      &&  make &&  make install &&  make install-docs \
      &&  cd contrib \
      &&  make &&  make install

VOLUME ${PG_HOME}
