FROM syndbg/postgres-xl-base

ENV PG_GTM_PORT=6666
ENV PG_STANDBY_HOST=0.0.0.0
ENV PG_STANDBY_PORT=6666
FROM mziescha/postgres-xl-base

COPY init/gtm.sh /usr/local/bin/gtm.sh

ENTRYPOINT ["gtm.sh"]


# NOTE: Logging for really, really poor people.
# This workarounds Docker's issue with `/dev/stdout` logging
# not being possible without `root` user.
CMD mkfifo -m 600 /tmp/logpipe && \
    (cat <> /tmp/logpipe 1>&2 &) && \
    gtm \
    -D ${PG_DATA} \
    -h ${PG_STANDBY_HOST} \
    -n ${PG_STANDBY_NODE} \
    -p ${PG_STANDBY_PORT} \
    -l /tmp/logpipe \
    -i ${PG_GTM_HOST} \
    -q ${PG_GTM_PORT} \
    -s

EXPOSE ${PG_GTM_PORT}

HEALTHCHECK CMD curl -fs http://${PG_GTM_HOST}:${PG_GTM_PORT} || false
