* gestartet mit https://github.com/syndbg/docker-postgres-xl-debian
* versuch update auf v10r1
* build scripte auf ansible geschrieben
* docker-compose ... funktionierte nicht
    * kein cluster wollte laufen
* fehlersuche began...
* rueckbau von dingen die ich nicht verstand
* ok...ich installiere von
    * installations doku nochmal laaaaaangsam gelesen
* coordinator running (awesome)
* can not connect to GTM: No route to host
    * gmt container hingestellt
    *    add gtm master gtm localhost 6666 $dataDirRoot/gtm
* gmt + coordinator running (awesome!)
* create datanode immage
* gmt + coordinator + 1 data node running (awesome!!)
* let's write the running cluster into ansible

* docker run -it --rm -v /db/coordinator_1:/var/lib/postgresql -e GTM_HOST=$(docker inspect --format='{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' gtm) -e PG_COORDINATOR_NODE=coordinator_1 --name coordinator_1 mziescha/postgres-xl-datanode startup
* docker run -it --rm -v /db/data_1:/var/lib/postgresql -e GTM_HOST=$(docker inspect --format='{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' gtm) -e PG_DATA_NODE=data_1 --name data_1 mziescha/postgres-xl-datanode startup
* docker run -it --rm -v /db/data_2:/var/lib/postgresql -e GTM_HOST=$(docker inspect --format='{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' gtm) -e PG_DATA_NODE=data_2 --name data_2 mziescha/postgres-xl-datanode startup

* i need vm's to test
* i wrote an ansible file for 6 vms cloned by one

* use the kubernetes files from the course

* docker versions and kubernetes version are koliding so I set the direct docker version

* turn of swap! why?

* ok I will skipp kubernetes, 'for now'

* docker run -it -d -v /db/gtm:/var/lib/postgresql --net host --name gtm mziescha/postgres-xl-gtm startup --force

* docker run -it -d -v /db/coordinator_1:/var/lib/postgresql -e GTM_HOST=10.244.0.1 -e GMT_PORT=19666 --net host -e PG_COORD_NODE=coordinator_1 --name coordinator_1 mziescha/postgres-xl-coordinator startup --force

* alter datanode because by default it's a coordinator node

```
select 
    quote_ident(node_name) as node_name,
    (case node_type
        when 'C' then 'coordinator'
        when 'D' then 'datanode'
    end) as node_type,
    node_host,
    node_port,
    nodeis_primary,
    nodeis_preferred
from
    pgxc_node;
```

introduce the network

'CREATE NODE name WITH ( TYPE =  datanode, HOST = hostname, PORT = portnum)'

psql -h 172.17.0.1 -p ${COORDINATOR_PORT} -U postgres postgres -c "CREATE NODE ${PG_DATA_NODE} WITH ( TYPE = datanode, HOST = '$(hostname -i)', PORT = 5432)"

psql -h 172.17.0.1 -p ${COORDINATOR_PORT} -U postgres postgres -c "SELECT pgxc_pool_reload()"

psql -h 172.17.0.1 -p ${COORDINATOR_PORT} -U postgres postgres -c "select quote_ident(node_name) as node_name from pgxc_node WHERE node_host = 'localhost';"

psql -t -h 172.17.0.1 -p ${COORDINATOR_PORT} -U postgres postgres -c "select quote_ident(node_name) as node_name from pgxc_node WHERE node_host = 'localhost';"

echo $(psql -t -h 172.17.0.1 -p ${COORDINATOR_PORT} -U postgres postgres -c "select quote_ident(node_name) as node_name from pgxc_node WHERE node_host = 'localhost';")

psql -U postgres postgres -c "CREATE NODE $(psql -t -h 172.17.0.1 -p ${COORDINATOR_PORT} -U postgres postgres -c "select quote_ident(node_name) as node_name from pgxc_node WHERE node_host = 'localhost';") WITH ( TYPE = coordinator, HOST = '172.17.0.1', PORT = ${COORDINATOR_PORT})"

psql -U postgres postgres -c "SELECT pgxc_pool_reload()"
