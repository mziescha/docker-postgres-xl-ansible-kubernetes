#!/bin/bash

FILE=$PG_DATA/already-initialized
echo "$PG_DATA"

set -e

mkdir -p $PG_HOME/pgxc_ctl
mkdir -p $PG_DATA
chmod 775 $PG_HOME

chown -R $PG_USER:$PG_USER $PG_HOME

# NOTE: Create the transaction log directory before initdb
# is run (below) so the directory is owned by the correct user.
if [ "$POSTGRES_INITDB_WALDIR" ]; then
    mkdir -p "$POSTGRES_INITDB_WALDIR"
    chown -R "$PG_USER":"$PG_USER" "$POSTGRES_INITDB_WALDIR"
    chmod 700 "$POSTGRES_INITDB_WALDIR"
fi

if [ ! -f $FILE ]; then
    gosu $PG_USER initgtm -D ${PG_DATA} -Z gtm_proxy

    touch $PG_DATA/already-initialized
    chown "$PG_USER":"$PG_USER" $PG_DATA/already-initialized
fi

gosu $PG_USER "$@"