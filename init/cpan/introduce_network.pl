#!/usr/bin/env perl

use strict;
use warnings;

BEGIN {
    use FindBin;
    use lib "/lib";
    use App::PostgresXL;
}

my $s_pg_data_host = qx/hostname -i/;
chomp $s_pg_data_host;

sub main {

    my $o_app_postgres_xl = App::PostgresXL->new(
        sleep_timer  => 10,
        pg_user      => $ENV{PG_USER},
        pg_home      => $ENV{PG_HOME},
        pgdata       => $ENV{PGDATA},
        pg_data_node => $ENV{PG_DATA_NODE},
        pg_data_port => $ENV{PG_DATA_PORT},
        gtm_host     => $ENV{GTM_HOST},
        pg_gtm_port  => $ENV{PG_GTM_PORT},
        node_type    => 'datanode',
        pg_data_host => $s_pg_data_host,
    );
    $o_app_postgres_xl->observer(
        {
            username => 'postgres',
            database => 'postgres',
            host     => $ENV{COORDINATOR_HOST},
            port     => $ENV{COORDINATOR_PORT},
        },
        1
    );
}

main();

# {

#     list_node_properties => q~
#         SELECT
#             quote_ident(n.nspname) AS schema_name,
#             quote_ident(c.relname) AS table_name,
#             (
#                 case x.pclocatortype
#                     when 'R' then 'replication'
#                     when 'N' then 'roundrobin'
#                     when 'H' then 'hash (' || a.attname || ')'
#                     when 'M' then 'modulo (' || a.attname || ')'
#                 end
#             ) AS distributed_by,
#             ( t.num_nodes = d.num_nodes ) AS all_nodes
#         FROM pgxc_class x
#         INNER JOIN pg_class c
#             ON (c.oid = x.pcrelid)
#         INNER JOIN pg_namespace n
#             ON (n.oid = c.relnamespace)
#         LEFT JOIN pg_attribute a
#             ON (a.attrelid = c.oid)
#         AND a.attnum = x.pcattnum
#         INNER JOIN (
#             SELECT
#                 t.pcrelid,
#                 count(*) AS num_nodes
#             FROM (
#                 SELECT
#                     pcrelid,
#                     unnest(nodeoids) AS nodeoid
#                 FROM
#                     pgxc_class
#                 ) t
#             GROUP BY t.pcrelid
#         ) t
#             ON (t.pcrelid = c.oid)
#         INNER JOIN (
#             SELECT
#                 count(*) AS num_nodes
#             FROM
#                 pgxc_node
#             WHERE
#                 node_type = 'D'
#         ) d
#             ON (1=1)
#         WHERE
#                 1=1
#             AND quote_ident(n.nspname) = '{0}'
#             AND quote_ident(c.relname) = '{1}'~,
#     lsit_node => q~
#         SELECT
#             quote_ident(t.schema_name) AS schema_name,
#             quote_ident(t.table_name) AS table_name,
#             quote_ident(n.node_name) AS node_name
#         FROM (
#             SELECT n.nspname AS schema_name,
#                 c.relname AS table_name,
#                 unnest(nodeoids) AS nodeoid
#             FROM
#                 pgxc_class x
#                 INNER JOIN pg_class c
#                     ON (c.oid = x.pcrelid)
#                 INNER JOIN pg_namespace n
#                     ON (n.oid = c.relnamespace)
#         ) t
#         INNER JOIN pgxc_node n
#             ON (n.oid = t.nodeoid)
#         WHERE
#                 TRUE
#             AND quote_ident(t.schema_name) = '{0}' AND quote_ident(t.table_name) = '{1}'
# ~,
#     clean_connection =>
# q~CLEAN CONNECTION TO --COORDINATOR ( nodename [, ... ] ) --NODE ( nodename [, ... ] ) --ALL --ALL FORCE --FOR DATABASE database_name --TO USER role_name~,
#     create_node =>
#       q~CREATE NODE name WITH ( TYPE = {{ coordinator | datanode }}, HOST = hostname, PORT = portnum --, PRIMARY --, PREFERRED )~,
#     alter_node =>
# q~ALTER NODE #node_name# WITH ( TYPE = {{ coordinator | datanode }}, HOST = hostname, PORT = portnum --, PRIMARY --, PREFERRED )~,
#     alter_table_distribution =>
#       q~ALTER TABLE #table_name# DISTRIBUTE BY --REPLICATION --ROUNDROBIN --HASH ( column_name ) --MODULO ( column_name )~,
# };

# ################################################################################
