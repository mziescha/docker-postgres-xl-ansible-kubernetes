#!/bin/bash

set -e

main(){

    cd ${PG_HOME}
    chmod 775 ${PG_HOME}
    chown -R ${PG_USER}:${PG_GROUP} ${PG_HOME}

    if [ ! -d "${PGDATA}" ]; then
        gosu ${PG_USER} mkdir -p ${PGDATA}
        chown -R ${PG_USER}:${PG_GROUP} ${PGDATA}
        chmod 700 ${PGDATA}
    fi
    case "$1" in
        'bash')
            gosu ${PG_USER} "$@"
        ;;
        'startup')
            shift
            init_db "$@"
            run "$@"
        ;;
        'init')
            shift
            init_db "$@"
        ;;
        'run')
            shift
            run "$@"
        ;;
    esac

    exit 0
}

run(){
    LOGFILE=${PG_HOME}/logfile
    gosu $PG_USER touch ${LOGFILE}
    gosu $PG_USER gtm_ctl -Z gtm -D ${PGDATA} -l ${LOGFILE} start
    tail -f ${LOGFILE}
}

init_db(){
    PARAM="$1"
    shift

    INIT_FILE="${PGDATA}/.ackinit"
    if [ ! -z "${PARAM}" ] && [ "${PARAM}" == '--force' ]; then
        rm -rf ${PGDATA}/* ${INIT_FILE}
    fi
    # NOTE: Create the transaction log directory before initdb
    # is run (below) so the directory is owned by the correct user.
    if [ "${POSTGRES_INITDB_XLOGDIR}" ]; then
        mkdir -p ${POSTGRES_INITDB_XLOGDIR}
        chown -R ${PG_USER}:${PG_GROUP} ${POSTGRES_INITDB_XLOGDIR}
        chmod 700 ${POSTGRES_INITDB_XLOGDIR}
    fi

    if [ ! -f $INIT_FILE ]; then
        gosu $PG_USER initgtm -D ${PGDATA} -Z gtm
        touch $INIT_FILE
        chown ${PG_USER}:${PG_GROUP} $INIT_FILE
    fi
}

main "$@"