#!/bin/bash

set -e

main(){

    cd ${PG_HOME}
    chmod 775 ${PG_HOME}
    chown -R ${PG_USER}:${PG_GROUP} ${PG_HOME}

    if [ ! -d "${PGDATA}" ]; then
        gosu ${PG_USER} mkdir -p ${PGDATA}
        chown -R ${PG_USER}:${PG_GROUP} ${PGDATA}
        chmod 700 ${PGDATA}
    fi
    echo "$1"
    case "$1" in
        'bash')
            gosu ${PG_USER} "$@"
        ;;
        'startup')
            shift
            init_db "$@"
            run "$@"
        ;;
        'init')
            shift
            init_db "$@"
        ;;
        'run')
            shift
            run "$@"
        ;;
    esac

    exit 0
}

run(){
    echo "run:  $1"
    LOGFILE=${PG_HOME}/logfile
    PG_BIN=$(which postgres)
    
    echo "${PG_BIN} --coordinator -D ${PGDATA}"
    
    gosu ${PG_USER} ${PG_BIN} --coordinator -D ${PGDATA}
}

add_pg_hba_entry() {
    echo "add_pg_hba_entry:  $1"
    network=$1
    LINE="host all all ${network} trust"
    HBA_FILE=${PGDATA}/pg_hba.conf

    grep -q "${LINE}" ${HBA_FILE} || echo ${LINE} >> ${HBA_FILE}
    return 0
}

init_db(){
    echo "init_db:  $1"
    PARAM="${1}"
    INIT_FILE="${PGDATA}/.ackinit"
    if [ ! -z "${PARAM}" ] && [ "${PARAM}" == '--force' ]; then
        shift
        rm -rf ${PGDATA}/* ${INIT_FILE}
    fi
    echo "init_db:  $1"

    # NOTE: Create the transaction log directory before initdb
    # is run (below) so the directory is owned by the correct user.
    if [ "${POSTGRES_INITDB_XLOGDIR}" ]; then
        mkdir -p ${POSTGRES_INITDB_XLOGDIR}
        chown -R ${PG_USER}:${PG_GROUP} ${POSTGRES_INITDB_XLOGDIR}
        chmod 700 ${POSTGRES_INITDB_XLOGDIR}
    fi

    if [ ! -f ${INIT_FILE} ]; then
        echo "${PG_USER} initdb -D ${PGDATA} --nodename=${PG_COORD_NODE}"
        gosu ${PG_USER} initdb -D ${PGDATA} --nodename=${PG_COORD_NODE}

        # NOTE: Allow connectivity between containers in the same Docker network.
        DOCKER_NETWORK=$(ip route show | tail +2 | cut -c-13)
        add_pg_hba_entry ${DOCKER_NETWORK}

        # NOTE: Opt-in to allow more IPs/networks if needed.
        # This is useful in a "real" production-ready environment
        # that spans across more than 1 Docker network, VM and/or host.
        if [ -z ${DOCKER_NETWORK_OPT_1+x} ]; then
            echo "skipping DOCKER_NETWORK_OPT_1"
        else
            add_pg_hba_entry ${DOCKER_NETWORK_OPT_1}
        fi

        if [ -z ${DOCKER_NETWORK_OPT_2+x} ]; then
            echo "skipping DOCKER_NETWORK_OPT_2"
        else
            add_pg_hba_entry ${DOCKER_NETWORK_OPT_2}
        fi

        if [ -z ${DOCKER_NETWORK_OPT_3+x} ]; then
            echo "skipping DOCKER_NETWORK_OPT_3"
        else
            add_pg_hba_entry ${DOCKER_NETWORK_OPT_3}
        fi

        if [ -z ${DOCKER_NETWORK_OPT_4+x} ]; then
            echo "skipping DOCKER_NETWORK_OPT_4"
        else
            add_pg_hba_entry ${DOCKER_NETWORK_OPT_4}
        fi
        echo "listen_addresses = '*'" >> ${PGDATA}/postgresql.conf
        echo "gtm_host = '${GTM_HOST}'" >> ${PGDATA}/postgresql.conf
        echo "gtm_port = ${PG_GTM_PORT}" >> ${PGDATA}/postgresql.conf
        echo "port = ${PG_COORD_PORT}" >> ${PGDATA}/postgresql.conf

        echo "${PG_USER} touch ${INIT_FILE}"
        gosu ${PG_USER} touch ${INIT_FILE}
    fi   
}

main "$@"
