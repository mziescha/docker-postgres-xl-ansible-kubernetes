package App::PostgresXL::CLI;

use parent 'App::PostgresXL';

sub new {
    my $s_class = shift;
    my $self = $s_class->SUPER::new(@_);
    $hr_params->{sleep_timer} //= 60;
    return bless $self, $s_class;
}

sub _list_node {
    my ($self) = @_;
    my $hr_conn_params =
      $self->{from_local}
      ? { username => 'postgres', database => 'postgres', host => 'localhost' }
      : { username => 'postgres', database => 'postgres', host => $self->{env}->{host}, port => $self->{env}->{port} };
    my $ar_nodes = $self->list_nodes( $self->_connection($hr_conn_params) );
    use DDP;
    p $ar_nodes;
}

sub _add_node {
    my ($self) = @_;
    $self->create_node

}

sub _remove_node {
    my ($self) = @_;

}

sub node {
    my ($self) = @_;
    $self->_add_node() if delete $self->{add};
    $self->_list_node() if delete $self->{list};
    $self->_remove_node() if delete $self->{remove};
}

sub group {
    my ($self) = @_;

}

sub loop {
    my ($self) = @_;

    do {
        $self->update_nodes($hr_db_params);
        sleep $self->{sleep_timer};
    } while ($self->{sleep_timer});
}

1;
