package App::PostgresXL;

use DBI;
use strict;
use warnings;

our $VERSION = '0.000100';

sub new {
    my $s_class   = shift;
    my $hr_params = {@_};
    return bless {@_}, $s_class;
}

sub _connection {
    my ( $self, $hr_db_params ) = @_;
    my $s_database     = $hr_db_params->{database};
    my $s_host         = $hr_db_params->{host};
    my $i_port         = $hr_db_params->{port};
    my $s_username     = $hr_db_params->{username};
    my $hr_conn_params = {
        PrintError => 1,
        RaiseError => 1,
    };

    if ( $s_username && $i_port ) {
        $self->{dbh} //=
          DBI->connect( "DBI:Pg:dbname=$s_database;host=$s_host;port=$i_port", $s_username, undef, $hr_conn_params );
    }
    elsif ( $s_username && !$i_port ) {
        $self->{dbh} //= DBI->connect( "DBI:Pg:dbname=$s_database;host=$s_host", $s_username, undef, $hr_conn_params );
    }
    elsif ( !$s_username && $i_port ) {
        $self->{dbh} //= DBI->connect( "DBI:Pg:dbname=$s_database;host=$s_host;port=$i_port", undef, undef, $hr_conn_params );
    }
    else {
        $self->{dbh} //= DBI->connect( "DBI:Pg:dbname=$s_database;host=$s_host", undef, undef, $hr_conn_params );
    }
    return $self->{dbh};
}

sub create_node {
    my ( $self, $o_dbh, $hr_node_params ) = @_;

    my $s_node_name = $hr_node_params->{name};
    my $s_node_type = $hr_node_params->{type};
    my $s_node_host = $hr_node_params->{host};
    my $s_node_port = $hr_node_params->{port};
    my $s_sql       = qq~CREATE NODE $s_node_name WITH ( TYPE = $s_node_type, HOST = '$s_node_host', PORT = $s_node_port)~;

    eval { $o_dbh->do($s_sql); };
    if ($@) {
        print $s_sql, ', ', $@;
    }
    return $self->pool_reload($o_dbh);
}

sub drop_node {
    my ( $self, $o_dbh, $s_node_name ) = @_;
    return unless $s_node_name;

    my $s_sql = qq~DROP NODE $s_node_name~;
    eval { $o_dbh->do($s_sql); };
    if ($@) {
        print $s_sql, ', ', $@;
    }
    return $self->pool_reload($o_dbh);
}

sub create_group { my ( $self, $o_dbh, $hr_params ) = @_; return q~CREATE NODE GROUP name WITH ( nodename [, ... ] )~, }
sub drop_group   { my ( $self, $o_dbh, $hr_params ) = @_; return q~DROP NODE GROUP #group_name#~, }

sub list_roups {
    my ( $self, $o_dbh, $hr_params ) = @_;
    return q~ SELECT quote_ident(group_name) AS group_name FROM pgxc_group ~,;
}
sub pause_cluster   { my ( $self, $o_dbh, $hr_params ) = @_; return 'PAUSE CLUSTER', }
sub unpause_cluster { my ( $self, $o_dbh, $hr_params ) = @_; return 'UNPAUSE CLUSTER', }

sub pool_reload {
    my ( $self, $o_dbh ) = @_;
    return $o_dbh->do(qq~SELECT pgxc_pool_reload()~);
}

sub alter_table_location {
    my ( $self, $o_dbh, $hr_params ) = @_;
    return q~ALTER TABLE #table_name# TO NODE ( nodename [, ... ] ) --TO GROUP ( groupname [, ... ] )~,;
}
sub alter_table_add_node { my ( $self, $o_dbh, $hr_params ) = @_; return q~ALTER TABLE #table_name# ADD NODE (node_name)~, }

sub alter_table_delete_node {
    my ( $self, $o_dbh, $hr_params ) = @_;
    return q~ALTER TABLE #table_name# DELETE NODE (#node_name#)~,;
}

sub list_group_nodes {
    my ( $self, $o_dbh, $s_group_name ) = @_;
    return $o_dbh->selectall_arrayref(
        q~SELECT
              quote_ident(n.node_name) AS node_name
          FROM
              (
                  SELECT
                      unnest(group_members) AS group_member
                  FROM
                      pgxc_group
                  WHERE
                      group_name = ?
              ) g
              INNER JOIN pgxc_node n
                  ON (n.oid = g.group_member)~,
        { Slice => {} }, $s_group_name

    );
}

sub check_node {
    my ( $self, $dbh ) = @_;
    return $dbh->do('SELECT 1') ? 1 : 0;
}

sub update_nodes {
    my ( $self, $hr_db_params ) = @_;
    my $s_coord_1_host = $hr_db_params->{host};
    my $s_coord_1_port = $hr_db_params->{port};
    my $o_dbh_coord    = $self->_connection($hr_db_params);
    my $ar_nodes       = $self->list_nodes($o_dbh_coord);

    for my $hr_node ( @{$ar_nodes} ) {
        if ( $hr_node->{node_host} eq 'localhost' ) {
            $hr_node->{node_host} = $s_coord_1_host;
            $hr_node->{node_port} = $s_coord_1_port;
        }
        my $o_dbh_remote = $self->_connection(
            {
                database => $hr_db_params->{database},
                host     => $hr_node->{node_host},
                port     => $hr_node->{node_port},
                username => $hr_db_params->{username},
            }
        );

        my $o_dbh_local = $self->_connection(
            {
                database => $hr_db_params->{database},
                host     => $self->{pg_data_host},
                port     => $self->{pg_data_port},
                username => $hr_db_params->{username},
            }
        );

        if ( $o_dbh_remote && $self->check_node($o_dbh_remote) ) {

            $self->create_node(
                $o_dbh_remote,
                {
                    name => $self->{pg_data_node},
                    type => $self->{node_type},
                    host => $self->{pg_data_host},
                    port => $self->{pg_data_port},
                }
            );

            $self->create_node(
                $o_dbh_local,
                {
                    name => $hr_node->{node_name},
                    type => $hr_node->{node_type},
                    host => $hr_node->{node_host},
                    port => $hr_node->{node_port},
                }
            );
        }
        else {
            $self->drop_node( $o_dbh_local, $hr_node->{node_name} );
        }
        $o_dbh_local->disconnect()  if $o_dbh_local;
        $o_dbh_remote->disconnect() if $o_dbh_remote;

        # db cooldown
        sleep 5;
    }

    $o_dbh_coord->disconnect();
}

sub list_nodes {
    my ( $self, $o_dbh ) = @_;
    return $o_dbh->selectall_arrayref(
        q~SELECT
            quote_ident(node_name) AS node_name,
            (case node_type
                when 'C' then 'coordinator'
                when 'D' then 'datanode'
            end) AS node_type,
            node_host,
            node_port,
            nodeis_primary,
            nodeis_preferred
        FROM
            pgxc_node~,
        { Slice => {} }

    );
}

1;    # Magic true value required at end of module
__END__

=head1 NAME

App::PostgresXL - [One line description of module's purpose here]


=head1 VERSION

This document describes App::PostgresXL version 0.0.1


=head1 SYNOPSIS

    use App::PostgresXL;

=for author to fill in:
    Brief code example(s) here showing commonest usage(s).
    This section will be as far as many users bother reading
    so make it as educational and exeplary as possible.
  
  
=head1 DESCRIPTION

=for author to fill in:
    Write a full description of the module and its features here.
    Use subsections (=head2, =head3) as appropriate.


=head1 INTERFACE 

=for author to fill in:
    Write a separate section listing the public components of the modules
    interface. These normally consist of either subroutines that may be
    exported, or methods that may be called on objects belonging to the
    classes provided by the module.


=head1 DIAGNOSTICS

=for author to fill in:
    List every single error and warning message that the module can
    generate (even the ones that will "never happen"), with a full
    explanation of each problem, one or more likely causes, and any
    suggested remedies.

=over

=item C<< Error message here, perhaps with %s placeholders >>

[Description of error here]

=item C<< Another error message here >>

[Description of error here]

[Et cetera, et cetera]

=back


=head1 CONFIGURATION AND ENVIRONMENT

=for author to fill in:
    A full explanation of any configuration system(s) used by the
    module, including the names and locations of any configuration
    files, and the meaning of any environment variables or properties
    that can be set. These descriptions must also include details of any
    configuration language used.
  
App::PostgresXL requires no configuration files or environment variables.


=head1 DEPENDENCIES

=for author to fill in:
    A list of all the other modules that this module relies upon,
    including any restrictions on versions, and an indication whether
    the module is part of the standard Perl distribution, part of the
    module's distribution, or must be installed separately. ]

None.


=head1 INCOMPATIBILITIES

=for author to fill in:
    A list of any modules that this module cannot be used in conjunction
    with. This may be due to name conflicts in the interface, or
    competition for system or program resources, or due to internal
    limitations of Perl (for example, many modules that use source code
    filters are mutually incompatible).

None reported.


=head1 BUGS AND LIMITATIONS

=for author to fill in:
    A list of known problems with the module, together with some
    indication Whether they are likely to be fixed in an upcoming
    release. Also a list of restrictions on the features the module
    does provide: data types that cannot be handled, performance issues
    and the circumstances in which they may arise, practical
    limitations on the size of data sets, special cases that are not
    (yet) handled, etc.

No bugs have been reported.

Please report any bugs or feature requests to
C<bug-app-postgresxl@rt.cpan.org>, or through the web interface at
L<http://rt.cpan.org>.


=head1 AUTHOR

Mario Zieschang  C<< <mziescha@cpan.org> >>


=head1 LICENCE AND COPYRIGHT

Copyright (c) 2019, Mario Zieschang C<< <mziescha@cpan.org> >>. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself. See L<perlartistic>.


=head1 DISCLAIMER OF WARRANTY

BECAUSE THIS SOFTWARE IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY
FOR THE SOFTWARE, TO THE EXTENT PERMITTED BY APPLICABLE LAW. EXCEPT WHEN
OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES
PROVIDE THE SOFTWARE "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER
EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE
ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE SOFTWARE IS WITH
YOU. SHOULD THE SOFTWARE PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL
NECESSARY SERVICING, REPAIR, OR CORRECTION.

IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING
WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY AND/OR
REDISTRIBUTE THE SOFTWARE AS PERMITTED BY THE ABOVE LICENCE, BE
LIABLE TO YOU FOR DAMAGES, INCLUDING ANY GENERAL, SPECIAL, INCIDENTAL,
OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OR INABILITY TO USE
THE SOFTWARE (INCLUDING BUT NOT LIMITED TO LOSS OF DATA OR DATA BEING
RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD PARTIES OR A
FAILURE OF THE SOFTWARE TO OPERATE WITH ANY OTHER SOFTWARE), EVEN IF
SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGES.
