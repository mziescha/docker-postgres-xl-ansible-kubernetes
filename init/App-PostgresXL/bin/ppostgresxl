#!/usr/bin/env perl
use strict;
use warnings;
use App::PostgresXL::CLI;
use Getopt::Long::Subcommand;
use Pod::Sub::Usage qw/sub2usage/;

my $s_pg_data_host = qx/hostname -i/;
chomp $s_pg_data_host;

our $VERSION = '0.010000';

my %commands = (
    node       => sub { App::PostgresXL::CLI->new(@_)->node },
    group      => sub { App::PostgresXL::CLI->new(@_)->group },
    loop       => sub { App::PostgresXL::CLI->new(@_)->loop },
    node_help  => sub { sub2usage( "node", "App::PostgresXL::CLI" ); },
    group_help => sub { sub2usage( "group", "App::PostgresXL::CLI" ); },
    loop_help  => sub { sub2usage( "loop", "App::PostgresXL::CLI" ); },
);

sub _h_process_command_line {

    my %opts = (
        argv => [@ARGV],
        env  => {
            pg_user      => $ENV{PG_USER},
            pg_home      => $ENV{PG_HOME},
            pgdata       => $ENV{PGDATA},
            pg_data_node => $ENV{PG_DATA_NODE},
            pg_data_port => $ENV{PG_DATA_PORT},
            gtm_host     => $ENV{GTM_HOST},
            pg_gtm_port  => $ENV{PG_GTM_PORT},
            node_type    => 'datanode',
            pg_data_host => $s_pg_data_host,
            host         => $ENV{COORDINATOR_HOST},
            port         => $ENV{COORDINATOR_PORT},
        }
    );

    my %options = (
        summary => 'Summary about your program ...',

        # common options recognized by all subcommands
        options => {
            'help|h|?' => {
                summary => 'Display help message',
                handler => sub {
                    my ( $cb, $val, $res ) = @_;
                    if ( $res->{subcommand} ) {
                        print "Help message for $res->{subcommand} ...";
                    }
                    else {
                        print "General help message ...";
                    }
                    exit 0;
                },
            },

            'version' => {
                summary => 'Display program version',
                handler => sub {
                    exit 0;
                },
            },

            'verbose' => { handler => \$opts{verbose}, },

            #in case of remote connection
            'remote-host=s' => \$opts{authority},
            'tls=s'         => \$opts{tls},
            'progress'      => \$opts{progress},
        },

        # list your subcommands here
        subcommands => {
            node => {
                summary => 'cli node commands',
                options => {
                    'add'           => \$opts{add},
                    'remove'        => \$opts{remove},
                    'list'          => \$opts{list},
                    'l|from-local'  => \$opts{from_local},
                    'd|detach-keys' => \$opts{detach_keys},
                    'n|no-stdin'    => \$opts{no_stdin},
                    's|sig-proxy'   => \$opts{sig_proxy},
                }
            },
            group => {
                summary => 'cli group commands',
                options => {
                    'c|container=s' => \$opts{container},
                }
            },
            loop => {
                summary => 'cli loop command',
                options => {
                    'add-host'     => \$opts{add_host},
                    'a|attach',    => \$opts{attach},
                    'blkio-weight' => \$opts{blkio_weight},

                },
            },
        },
    );

    my $res = GetOptions(%options);
    while ( my ( $key, $val ) = each %opts ) {
        delete $opts{$key} if not defined $val;
    }

    if ( !defined $res->{subcommand} || scalar @{ $res->{subcommand} } == 0 || !defined $commands{ $res->{subcommand}->[0] } ) {
        require Pod::Usage;
        Pod::Usage::pod2usage( { -verbose => 1 } );
        exit 0;
    }
    shift @{ $opts{argv} };
    $commands{ $res->{subcommand}->[0] }->(%opts) if $res->{success} && $commands{ $res->{subcommand}->[0] };
}

_h_process_command_line();

exit 0;

__END__

=pod

=head1 SYNOPSIS

Quick summary of what the module does.

Perhaps a little code snippet.

    $> ppostgresxl -h

    $> ppostgresxl -v

    $> ppostgresxl node -h

    $> ppostgresxl group -h

    $> ppostgresxl loop -h

=head1 AUTHOR

Mario Zieschang, C<< <mziescha at cpan.org> >>

=head1 BUGS

Please report any bugs or feature requests to C<bug-app-docker at rt.cpan.org>, or through
the web interface at L<http://rt.cpan.org/NoAuth/ReportBug.html?Queue=App-PostgresXL>.  I will be notified, and then you'll
automatically be notified of progress on your bug as I make changes.
=head1 SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc App::PostgresXL::CLI
You can also look for information at:

=over 4

=item * RT: CPAN's request tracker (report bugs here)

L<http://rt.cpan.org/NoAuth/Bugs.html?Dist=App-PostgresXL>

=item * AnnoCPAN: Annotated CPAN documentation

L<http://annocpan.org/dist/App-PostgresXL>

=item * CPAN Ratings

L<http://cpanratings.perl.org/d/App-PostgresXL>

=item * Search CPAN

L<http://search.cpan.org/dist/App-PostgresXL/>

=back

=head1 LICENSE AND COPYRIGHT


Copyright 2019 Mario Zieschang.

This program is free software; you can redistribute it and/or modify it
under the terms of the the Artistic License (2.0). You may obtain a
copy of the full license at:

L<http://www.perlfoundation.org/artistic_license_2_0>

Any use, modification, and distribution of the Standard or Modified
Versions is governed by this Artistic License. By using, modifying or
distributing the Package, you accept this license. Do not use, modify,
or distribute the Package, if you do not accept this license.

If your Modified Version has been derived from a Modified Version made
by someone other than you, you are nevertheless required to ensure that
your Modified Version complies with the requirements of this license.

This license does not grant you the right to use any trademark, service
mark, tradename, or logo of the Copyright Holder.

This license includes the non-exclusive, worldwide, free-of-charge
patent license to make, have made, use, offer to sell, sell, import and
otherwise transfer the Package with respect to any patent claims
licensable by the Copyright Holder that are necessarily infringed by the
Package. If you institute patent litigation (including a cross-claim or
counterclaim) against any party alleging that the Package constitutes
direct or contributory patent infringement, then this Artistic License
to you shall terminate on the date that such litigation is filed.

Disclaimer of Warranty: THE PACKAGE IS PROVIDED BY THE COPYRIGHT HOLDER
AND CONTRIBUTORS "AS IS' AND WITHOUT ANY EXPRESS OR IMPLIED WARRANTIES.
THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE, OR NON-INFRINGEMENT ARE DISCLAIMED TO THE EXTENT PERMITTED BY
YOUR LOCAL LAW. UNLESS REQUIRED BY LAW, NO COPYRIGHT HOLDER OR
CONTRIBUTOR WILL BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, OR
CONSEQUENTIAL DAMAGES ARISING IN ANY WAY OUT OF THE USE OF THE PACKAGE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=cut
